# proxy-for-terraform

NGINX proxy for terraform learning

## Usage

### Env Variables

 * `LISTEN_PORT` - Port ti listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_PORT` - Port of the app to forward to (default: `9000`)
